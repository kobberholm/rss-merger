# rss-merger

This program is based on [uniphil /
merge-rss-php](https://github.com/uniphil/merge-rss-php/blob/master/composer.json)

I have wrapped the functions in a class, added a ton of getters and setters and made it possible to change the rss stub, that is used as the base of the XML document.

Also, I have not made any tests, but you are free to do so :)

## Install with Composer
1. Make sure you have [composer](https://getcomposer.org/) installed.
2. Clone this repo:
```
git clone https://gitlab.com/kobberholm/rss-merger.git
```
3. Run `composer install`.