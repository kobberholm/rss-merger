<?php
require_once('vendor/autoload.php');


$feeds = [
'thisweekinlinux' => [
    'url' => 'https://tuxdigital.com/feed/thisweekinlinux-mp3',
    'title' => 'This Week In Linux'
    ],
'hardwareaddicts' => [
    'url' => 'https://hardwareaddicts.org/rss',
    'title' => 'Hardware Addicts'
],
'dlnxtend' => [
    'url' => 'https://dlnxtend.com/rss',
    'title' => 'DLNXtend'
],
'sudoshow' => [
    'url' => 'https://sudo.show/rss',
    'title' => 'Sudo Show'
],
'linux4everyone' => [
    'url' => 'https://www.linux4everyone.com/rss',
    'title' => 'Linux 4 Everyone'
],
'destinationlinux' => [
    'url' => 'https://destinationlinux.org/feed/mp3',
    'title' => 'Destination Linux'
],
'asknoahshow' => [
    'url' => 'https://podcast.asknoahshow.com/rss',
    'title' => 'Ask Noah Show'
    ]
];



$merger = new DLN\RSSMerger();

$merger->setStub(
    '<?xml version="1.0" encoding="utf-8"?>
    <rss
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
        xmlns:fireside="http://fireside.fm/modules/rss/fireside"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        xmlns:rawvoice="http://www.rawvoice.com/rawvoiceRssModule/"
        version="2.0">
      <channel>
      <generator>DLN RSS Merge</generator>
      <title>All Shows on Destination Linux Network</title>
      <link>http://destinationlinux.network/all-shows-feed</link>
      <pubDate></pubDate>
      <description>All shows of the Destination Linux Network combined into a single rss feed.</description>
      <language>en</language>
      <itunes:type>episodic</itunes:type>
      <itunes:author>Destination Linux Network</itunes:author>
      <itunes:summary>All shows of the Destination Linux Network combined into a single rss feed.</itunes:summary>
      <itunes:image href=""/>
      <itunes:explicit>no</itunes:explicit>
      <itunes:owner>
        <itunes:name>Destination Linux Network</itunes:name>
        <itunes:email>mail@destinationlinux.network</itunes:email>
      </itunes:owner>
      </channel>
    </rss>');


$merger->addFeeds($feeds);

// If run from a web page, add content-type header
if( !empty($_SERVER['REMOTE_ADDR']) || isset($_SERVER['HTTP_USER_AGENT']) || count($_SERVER['argv']) == 0) {
    // Cache the page for 10 minutes
    $seconds_to_cache = 600;
    $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
    header("Expires: $ts");
    header("Pragma: cache");
    header("Cache-Control: max-age=$seconds_to_cache");
    header('Content-Type: application/rss+xml; charset=utf-8');
}

echo $merger->getOutputFeedAsXML();
?>