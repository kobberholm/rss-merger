<?php

namespace DLN;


use \SimpleXMLElement;
use \SimplePie_Misc;

class RSSMerger
{
    // Set a default feedstub
    protected $stub =
    '<?xml version="1.0"?>
    <rss version="2.0">
        <channel>
        </channel>
    </rss>';
    
    protected $feeds = [];
    
    protected $feedsData = [];

    protected $outputFeedObject;

    protected $allDataIsCurrent = true;
    
    public function setStub(string $stub)
    {
        $this->stub = $stub;
        return $this;
    }

    public function getStub()
    {
        return $this->stub;
    }

    // Add a feed to the internal feed list
    public function addFeed(string $key, array $feed)
    {
        $this->feeds[$key] = array_merge($feed, ['data_is_current' => false]);
        $this->setDataStale();
        return $this;
    }
    
    // Add an array of feeds to the internal feed list
    public function addFeeds($feeds)
    {
        foreach($feeds as $feedKey => $feed) {
            $this->addFeed($feedKey, $feed);
        }
        return $this;
    }
    
    public function loadFeeds()
    {
        // Don't do anything, if data is current
        if($this->isAllDataCurrent()) {
            return;
        }
        $this->resetData();

        // Loop through the feeds and load data from the URL
        foreach($this->feeds as &$feed) {
            // Only load data if feed has been added since last load
            if(!$this->isAllDataCurrent() && !$feed['data_is_current']) {
                $data = simpleXML_load_file($feed['url'],"SimpleXMLElement");
                $feed['data_is_current'] = true;
                if($data !== false) {
                    // Feed data loaded succesfully. Assign it to the data array
                    $this->feedData[] = $data;
                }
            }

        }
        // Mark all data as current
        $this->setDataCurrent();
        return $this;
    }

    public function isAllDataCurrent() : bool
    {
        return $this->allDataIsCurrent;
    }

    public function setDataStale()
    {
        $this->allDataIsCurrent = false;
        return $this;
    }

    public function setDataCurrent()
    {
        $this->allDataIsCurrent = true;
        return $this;
    }

    public function resetFeeds()
    {
        $this->feeds = [];
        $this->resetData();
        return $this;
    }

    public function resetData()
    {
        $this->feedsData = [];
        $this->outputFeedObject = null;
        return $this;
    }

    // try several alternative paths to the thing we want
    // woo rss and all its variety of ways to mark up a document
    protected function findXMLFrom($searches, $xml) {
        foreach ($searches as $path) {
            $tmp = $xml;
            foreach ($path as $ns => $tag_name) {
                if (is_string($ns)) {
                    $tmp = $tmp->children($ns, true)->{$tag_name};
                } else {
                    $tmp = $tmp->{$tag_name};
                }
            }
            if (!empty($tmp)) {
                break;
            }
        }
        return $tmp;
    }


    protected function getItemDate($item) {
        $date = $this->findXMLFrom(array(
            array('pubDate'),
            array('published'),
            array('dc' => 'date'),
        ), $item);
        return SimplePie_Misc::parse_date($date);
    }


    public function mergeRSS() {
        // 1. set up a standard xml structure
        $out = new SimpleXMLElement($this->stub);

        if(!$this->isAllDataCurrent()) {
            $this->loadFeeds();
        } else {
            // If data is current and an output object is set, simply return it
            if($outputObject =& $this->getOutputFeedObject()) {
                return $outputObject;
            }
        }

        // 2. pull all feed items out into a big flat array
        $items = array();
        foreach ($this->feedData as $feed) {
            $things = $this->findXMLFrom(array(
                array("channel", "item"),
                array("entry"),
            ), $feed);

            // don't die on bad feeds missing <channel>
            // just skip the feed instead
            if (empty($things)) {
                continue;
            }

            foreach ($things as $item) {
                $items[] = $item;
            }
        }

        // 3. sort the items by date
        usort($items, function($a, $b) {
            return $this->getItemDate($b) - $this->getItemDate($a);
        });

        // 4. stick them in the output feed
        // SimpleXML doesn't do tree splicing, we need DOM for that
        $out_channel = $out->channel;
        $out_channel_dom = dom_import_simplexml($out_channel);
        foreach ($items as $item) {
            $to_add_dom = $out_channel_dom->ownerDocument->importNode(dom_import_simplexml($item), true);
            $out_channel_dom->appendChild($to_add_dom);
        }
        $this->setOutputFeedObject($out);
    }

    public function setOutputFeedObject(&$domObject)
    {
        $this->outputFeedObject =& $domObject;
        return $this;
    }

    public function getOutputFeedObject()
    {
        return $this->outputFeedObject;
    }

    public function getOutputFeedAsXML()
    {
        $this->mergeRSS();

        return $this->getOutputFeedObject()->asXML();
    }

}
